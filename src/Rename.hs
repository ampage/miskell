module Main where

import Options.Applicative
import Data.Semigroup ((<>))

import System.Directory
import System.Exit
import Control.Monad
import Control.Monad.Extra
import Data.List
import Text.Printf

data Options = Options
  { parentDir :: FilePath
  , baseName :: String
  , startNum :: Int
  , dryRun :: Bool }

main :: IO ()
main = run =<< execParser options

optionsParser :: Parser Options
optionsParser = Options
  <$> argument str (metavar "DIR"
                    <> help "Directory whose contents you want to rename.")
  <*> argument str (metavar "NAME"
                   <> help ("The desired new name for all files (minus " ++
                            "numeric suffix and extension, if present)."))
  <*> option auto (long "start-num"
                   <> short 'n'
                   <> metavar "N"
                   <> value 0
                   <> help ("The number from which the numerical filename " ++
                            "suffixes should start counting up."))
  <*> switch (long "dry-run"
             <> help ("Do everything except actually modify the files."))

options :: ParserInfo Options
options = info (optionsParser <**> helper)
  (fullDesc
   <> progDesc "Rename all files in a directory according to specified pattern."
   <> header "rename - a script for renaming all files in a directory")

getRootAndExtension :: FilePath -> (String, String)
getRootAndExtension = span (/= '.')

getRoot :: FilePath -> String
getRoot = fst . getRootAndExtension

getExtension :: FilePath -> String
getExtension = snd . getRootAndExtension

newNames :: String -> Int -> [FilePath] -> [FilePath]
newNames base start filenames =
  zipWith newName filenames $ numericSuffixes (length filenames) start
  where
    newName filename suffix = base ++ suffix ++ (getExtension filename)

numericSuffixes :: Int -> Int -> [String]
numericSuffixes total initial = map f [initial..]
  where
    numDigits = ceiling (logBase 10 $ fromIntegral (total + initial))
    f x = replicate (numDigits - (length (show x))) '0' ++ (show x)

elide :: Int -> String -> String
elide n xs
  | (length xs) >= (n - 5) = take (n - (5 + (length extension))) xs ++
                             "[...]" ++ extension
  | otherwise = xs
  where extension = getExtension xs

renameText :: String -> String -> String
renameText old new = printf "  %-40s -> %-40s\n" (elide 40 old) (elide 40 new)

run :: Options -> IO ()
run opts = withCurrentDirectory (parentDir opts) (do
  dirContents <- listDirectory "."
  filenames <- liftM sort (filterM doesFileExist dirContents)
  let newnames = (newNames (baseName opts) (startNum opts) filenames)
      output = concat (zipWith renameText filenames newnames)
  if (dryRun opts)
  then do putStrLn "DRY RUN:"
          putStrLn output
  else ifM (anyM doesFileExist newnames)
       (die "Aborted: a file already exists at a target location.")
       (do zipWithM_ renameFile filenames newnames
           putStrLn output))
